import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthModule} from './auth/auth.module';
import {AuthGuard} from './auth/auth-guard';
import {HomeModule} from './home/home.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/content',
    pathMatch: 'full'
  },
  {
    path: 'content',
    loadChildren: () => HomeModule,
    canActivateChild: [AuthGuard]
  },
  {
    path: '',
    loadChildren: () => AuthModule
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
