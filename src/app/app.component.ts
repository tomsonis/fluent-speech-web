import {Component} from '@angular/core';
import {AppToastService} from './shared/services/app-toast.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'fluent-speech';

  constructor(
    public toastService: AppToastService
  ) {}

}
