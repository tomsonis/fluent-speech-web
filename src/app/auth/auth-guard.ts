import {ActivatedRouteSnapshot, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {AuthUserService} from './auth-user.service';

@Injectable()
export class AuthGuard implements CanLoad, CanActivateChild {

  constructor(
    private router: Router,
    private authUserService: AuthUserService
  ) {
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authUserService.isLogged() ? true : this.router.parseUrl('login');

  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authUserService.isLogged() ? true : this.router.parseUrl('login');
  }
}
