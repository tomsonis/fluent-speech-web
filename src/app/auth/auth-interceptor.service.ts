import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AppToastService} from '../shared/services/app-toast.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private toastService: AppToastService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      withCredentials: true
    });
    return next.handle(request).pipe(
      tap(
        _ => {},
        error => this.onError(error)
      )
    );
  }

  private onError(error: any): void {
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401 && this.router.url !== '/login') {
        this.router.navigate(['/login']);
      } else if (error.status >= 500) {
        this.toastService.show('Wystąpił błąd. Spróbuj ponownie później', {classname: 'bg-danger text-light', delay: 3000});
      }
    }
  }
}
