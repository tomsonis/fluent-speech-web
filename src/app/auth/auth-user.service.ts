import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BASE_URL} from '../shared/const';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserDetails} from './user.model';
import {tap} from 'rxjs/operators';
import {UserRegister} from './register/UserRegister';

@Injectable({providedIn: 'root'})
export class AuthUserService {

  private loggedUser = new BehaviorSubject<UserDetails>(null);

  constructor(private http: HttpClient) {
    this.readUserContext();
  }

  isLogged(): boolean {
    return !!this.loggedUser.getValue();
  }

  login(username: string, password: string): Observable<UserDetails> {
    const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(username + ':' + password)});
    return this.http.get<UserDetails>(`${BASE_URL}/users/authenticate`, {headers})
      .pipe(
        tap((userDetails: UserDetails) => {
          localStorage.setItem('user', JSON.stringify(userDetails));
          this.loggedUser = new BehaviorSubject<UserDetails>(userDetails);
        })
      );
  }

  logout(): Observable<any> {
    return this.http.get<any>(`${BASE_URL}/logout`)
      .pipe(
        tap(_ => this.clearUserContext())
      );
  }

  private readUserContext(): void {
    const userDetails: UserDetails = JSON.parse(localStorage.getItem('user'));
    this.loggedUser = new BehaviorSubject<UserDetails>(userDetails);
  }

  private clearUserContext(): void {
    localStorage.setItem('user', null);
    this.loggedUser.next(null);
  }

  register(userRegister: UserRegister): Observable<UserDetails> {
    return this.http.post<UserDetails>(`${BASE_URL}/users/register`, userRegister);
  }
}
