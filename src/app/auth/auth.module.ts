import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginViewComponent} from './login/login-view/login-view.component';
import {RegisterComponent} from './register/register.component';
import {RegisterFormComponent} from './register/register-form/register-form.component';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

const AuthRotes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  declarations: [LoginComponent, LoginViewComponent, RegisterComponent, RegisterFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AuthRotes),
    ReactiveFormsModule,
    NgbAlertModule
  ]
})
export class AuthModule {
}
