import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Alert} from '../../../shared/shared.model';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginViewComponent {

  @Input()
  loginForm: FormGroup;

  @Input()
  alerts: Alert[] = [];

  @Output()
  onSubmitEvent = new EventEmitter<any>();

  @Output()
  onClearEvent = new EventEmitter<any>();

  onSubmit(): void {
    this.onSubmitEvent.emit();
  }

  clear(): void {
    this.onClearEvent.emit();
  }
}
