import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthUserService} from '../auth-user.service';
import {Router} from '@angular/router';
import {Alert} from '../../shared/shared.model';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  alerts: Alert[] = [];
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authUserService: AuthUserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    this.clearAlerts();

    if (this.loginForm.invalid) {
      return;
    }
    this.authUserService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value)
      .subscribe(
        _ => {
          this.router.navigate(['/content']);
        },
        _ => {
          this.addAlerts({type: 'danger', message: 'Invalid user or password'});
        }
      );
  }

  clearAlerts(): void {
    this.alerts = [];
  }

  private addAlerts(alert: Alert): void {
    this.alerts.push(alert);
  }
}
