import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterFormComponent implements OnInit {

  @Input()
  registerForm: FormGroup;

  @Input()
  isRegistered: boolean;

  @Output()
  onSubmitEvent = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {

  }

  onSubmit(): void {
    this.onSubmitEvent.emit();
  }
}
