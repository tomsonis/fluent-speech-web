import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthUserService} from '../auth-user.service';
import {UserRegister} from './UserRegister';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  isRegistered = false;
  constructor(
    private formBuilder: FormBuilder,
    private authUserService: AuthUserService
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    if (this.registerForm.invalid) {
      return;
    }
    const userRegister: UserRegister = {
      username: this.registerForm.controls.username.value,
      email: this.registerForm.controls.email.value,
      password: this.registerForm.controls.password.value
    };
    this.authUserService.register(userRegister)
      .subscribe(_ => {
        this.isRegistered = true;
      });
  }
}
