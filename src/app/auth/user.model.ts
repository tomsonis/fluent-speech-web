export interface UserDetails  {
  username: string;
  email: string;
  authorities: string[];
  enabled: boolean;
}
