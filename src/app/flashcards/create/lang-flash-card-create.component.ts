import {Component} from '@angular/core';
import {LangFlashCard} from '../langFlashCard';
import {LangFlashCardsService} from '../service/lang-flash-cards.service';
import {Lang} from '../../shared/const';

@Component({
  selector: 'app-phrase-create',
  templateUrl: './lang-flash-card-create.component.html'
})
export class LangFlashCardCreateComponent {

  langs = Object.keys(Lang);

  constructor(
    private langFlashCardsService: LangFlashCardsService
  ) {
  }

  addLangFlashCard(langFlashCard: LangFlashCard): void {
    this.langFlashCardsService.addLangFlashCard(langFlashCard)
      .subscribe(_ => {
      });
  }
}
