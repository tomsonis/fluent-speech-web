import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LangFlashCard} from '../../langFlashCard';

@Component({
  selector: 'app-flash-cards-create-view',
  templateUrl: './lang-flash-cards-create-view.component.html'
})
export class LangFlashCardsCreateViewComponent {

  @Input()
  langFlashCard = new LangFlashCard();

  @Input()
  langs: string[];

  @Output('action')
  actionEmitter = new EventEmitter<LangFlashCard>();

  addPhrase(): any {
    this.actionEmitter.emit(this.langFlashCard);
  }

  resetForm(): any {
    this.langFlashCard = new LangFlashCard();
  }
}
