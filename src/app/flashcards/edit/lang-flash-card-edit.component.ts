import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LangFlashCardsService} from '../service/lang-flash-cards.service';
import {Observable} from 'rxjs';
import {LangFlashCard} from '../langFlashCard';
import {Lang} from '../../shared/const';

@Component({
  selector: 'app-phrase-edit',
  templateUrl: './lang-flash-card-edit.component.html'
})
export class LangFlashCardEditComponent implements OnInit {

  langFlashCard$: Observable<LangFlashCard>;
  langs = Object.keys(Lang);

  constructor(
    private route: ActivatedRoute,
    private langFlashCardsService: LangFlashCardsService
  ) {
  }

  ngOnInit(): void {
    const flashCardId = this.route.snapshot.paramMap.get('flashCardId');
    this.langFlashCard$ = this.langFlashCardsService.langFlashCard(flashCardId);
  }

  updateLangFlashCard(langFlashCard: LangFlashCard): void {
    this.langFlashCard$ = this.langFlashCardsService.update(langFlashCard);
  }
}
