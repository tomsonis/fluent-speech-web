import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlashCardsComponent} from './flash-cards.component';
import {RouterModule, Routes} from '@angular/router';
import {LangFlashCardListComponent} from './list/lang-flash-card-list.component';
import {LangFlashCardCreateComponent} from './create/lang-flash-card-create.component';
import {LangFlashCardEditComponent} from './edit/lang-flash-card-edit.component';
import {SharedModule} from '../shared/shared.module';
import {LangFlashCardsListViewComponent} from './list/view/lang-flash-cards-list-view.component';
import {LangFlashCardsCreateViewComponent} from './create/view/lang-flash-cards-create-view.component';

const DictionaryRotes: Routes = [
  {
    path: '',
    component: FlashCardsComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: LangFlashCardListComponent
      },
      {
        path: 'add',
        component: LangFlashCardCreateComponent
      },
      {
        path: 'edit/:flashCardId',
        component: LangFlashCardEditComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    FlashCardsComponent,
    LangFlashCardListComponent,
    LangFlashCardCreateComponent,
    LangFlashCardEditComponent,
    LangFlashCardsCreateViewComponent,
    LangFlashCardsListViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(DictionaryRotes),
    SharedModule
  ]
})
export class FlashCardsModule {
}
