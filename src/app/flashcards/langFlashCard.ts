import {Lang} from '../shared/const';

export class LangFlashCard {
  id: string;
  firstSide: string;
  secondSide: string;
  lang: Lang = Lang.EN;
  comment: string;
  category: string;
  secondForm: string;
  thirdForm: string;
}
