import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {LangFlashCard} from '../langFlashCard';
import {DEFAULT_PAGE, DEFAULT_SIZE, Pageable} from '../../shared/const';
import {BehaviorSubject, Observable} from 'rxjs';
import {SearchRequest} from '../../shared/search/search.model';
import {switchMap} from 'rxjs/operators';
import {LangFlashCardsService} from '../service/lang-flash-cards.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {DeleteComponent} from '../../shared/modal/delete/delete.component';
import {ModalObject} from '../../shared/modal/ModalObject';

@Component({
  selector: 'app-lang-list',
  templateUrl: './lang-flash-card-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LangFlashCardListComponent implements OnInit {

  langFlashCards$: Observable<Pageable<LangFlashCard>>;
  search$ = new BehaviorSubject<SearchRequest>(new SearchRequest('', DEFAULT_PAGE));

  constructor(
    private langFlashCardsService: LangFlashCardsService,
    private modal: NgbModal,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.langFlashCards$ = this.search$.pipe(
      switchMap(searchData => this.langFlashCardsService.search(searchData.searchName, searchData.page, DEFAULT_SIZE))
    );
  }

  openDeleteModalConfirmation(deleteLangFlashCard: LangFlashCard): void {
    const ngbModalRef = this.modal.open(DeleteComponent);
    ngbModalRef.componentInstance.modalObject = new ModalObject(deleteLangFlashCard.firstSide);
    ngbModalRef.result.then(() => {
      this.langFlashCardsService.delete(deleteLangFlashCard.id)
        .subscribe(_ => this.fetchFlashCards(DEFAULT_PAGE));
    })
      .catch(_ => {
      });
  }

  redirectToEdit(id: string): void {
    this.router.navigateByUrl(`/content/flashcards/edit/${id}`);
  }

  fetchFlashCards(page: number): void {
    const lastValue = this.search$.getValue();
    this.search$.next(new SearchRequest(lastValue.searchName, page));
  }
}
