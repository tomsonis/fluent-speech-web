import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Pageable} from '../../../shared/const';
import {LangFlashCard} from '../../langFlashCard';

@Component({
  selector: 'app-flash-cards-list-view',
  templateUrl: './lang-flash-cards-list-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LangFlashCardsListViewComponent {

  @Input()
  langFlashCards: Pageable<LangFlashCard>;

  @Output()
  deleteEvent = new EventEmitter<LangFlashCard>();

  @Output()
  editEvent = new EventEmitter<any>();

  delete(langFlashCard: LangFlashCard): void {
    this.deleteEvent.emit(langFlashCard);
  }

  edit(id: string): void {
    this.editEvent.emit(id);
  }
}
