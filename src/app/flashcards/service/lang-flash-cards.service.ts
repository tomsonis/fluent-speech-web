import {Injectable} from '@angular/core';
import {LangFlashCard} from '../langFlashCard';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {BASE_URL, Pageable} from '../../shared/const';

@Injectable({
  providedIn: 'root'
})
export class LangFlashCardsService {

  constructor(private httpClient: HttpClient) {
  }

  addLangFlashCard(langFlashCard: LangFlashCard): Observable<any> {
    return this.httpClient.post<any>(`${BASE_URL}/lang-flash-cards`, langFlashCard);
  }

  getAll(page: number, size: number): Observable<Pageable<LangFlashCard>> {
    if (size < 0  || page < 1) {
      throwError('Invalid parameters.');
    }
    return this.httpClient.get<Pageable<LangFlashCard>>(`${BASE_URL}/lang-flash-cards?page=${page}&size=${size}`);
  }

  langFlashCard(id: string): Observable<LangFlashCard> {
    return this.httpClient.get<LangFlashCard>(`${BASE_URL}/lang-flash-cards/${id}`);
  }

  delete(id: string): Observable<any> {
    return this.httpClient.delete<Observable<any>>(`${BASE_URL}/lang-flash-cards/${id}`);
  }

  update(langFlashCard: LangFlashCard): Observable<LangFlashCard> {
    return this.httpClient.put<LangFlashCard>(`${BASE_URL}/lang-flash-cards`, langFlashCard);
  }

  search(searchName: string, page: number, size: number): Observable<Pageable<LangFlashCard>> {
    return this.httpClient.get<Pageable<LangFlashCard>>(`${BASE_URL}/lang-flash-cards/search?searchName=${searchName}&page=${page}&size=${size}`);
  }
}
