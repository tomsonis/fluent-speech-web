import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthUserService} from '../auth/auth-user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  constructor(
    private router: Router,
    private authUserService: AuthUserService
  ) {
  }

  logout(): void {
    this.authUserService.logout().subscribe(() => {
      this.router.navigate(['/login']);
    });
  }
}
