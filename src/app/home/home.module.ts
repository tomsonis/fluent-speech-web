import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeViewComponent} from './home-view/home-view.component';
import {FlashCardsModule} from '../flashcards/flash-cards.module';
import {AuthGuard} from '../auth/auth-guard';
import {RepetitionModule} from '../repetition/repetition.module';
import {HomeComponent} from './home.component';


const HomeRouter: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeViewComponent
      },
      {
        path: 'flashcards',
        loadChildren: () => FlashCardsModule,
        canLoad: [AuthGuard]
      },
      {
        path: 'repetition',
        loadChildren: () => RepetitionModule,
        canLoad: [AuthGuard]
      },
      {
        path: 'gramma',
        loadChildren: () => RepetitionModule,
        canLoad: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  declarations: [HomeViewComponent, HomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(HomeRouter)
  ],
  exports: []
})
export class HomeModule {
}
