import {Component, OnInit} from '@angular/core';
import {Repetition, RepetitionModel, RepetitionResponse} from '../model/Repetition';
import {Observable} from 'rxjs';
import {RepetitionService} from '../service/repetition.service';
import {Pageable} from '../../shared/const';

@Component({
  selector: 'app-repetition',
  templateUrl: './default-repetition.component.html'
})
export class DefaultRepetitionComponent implements OnInit {

  repetition$: Observable<RepetitionResponse<Pageable<Repetition>>>;

  constructor(private repetitionService: RepetitionService) {
  }

  ngOnInit(): void {
    this.loadRepetition();
  }

  saveRepetition(repetitionModel: RepetitionModel): void {
    this.repetitionService.saveRepetition(repetitionModel.repetitionAnswer)
      .subscribe(() => {
        if (repetitionModel.loadMore) {
          this.loadRepetition();
        }
      });
  }

  loadRepetition(): void {
    this.repetition$ = this.repetitionService.getDefaultRepetitions();
  }
}
