export class RepetitionAnswerModel {
  private _answered: boolean;
  private _valid: boolean;
  answer: string;

  get answered(): boolean {
    return this._answered;
  }

  get valid(): boolean {
    return this._valid;
  }

  clear(): void {
    this._answered = false;
    this._valid = false;
    this.answer = null;
  }

  validateAnswers(answer: string): boolean {
    this._answered = true;
    const correctAnswers = answer.split(',');

    const strings = correctAnswers
      .map(value => value.toLowerCase().trim());

    this._valid = this.answer && strings.indexOf(this.answer.toLowerCase().trim()) !== -1;
    return this._valid;
  }
}
