import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, Renderer2} from '@angular/core';
import {Repetition, RepetitionModel, RepetitionResponse} from '../../model/Repetition';
import {RepetitionAnswerModel} from '../repetition-model';
import {Pageable} from '../../../shared/const';

@Component({
  selector: 'app-default-repetition-view',
  templateUrl: './default-repetition-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultRepetitionViewComponent implements OnInit, OnDestroy {

  @Input()
  repetitionsPageable: RepetitionResponse<Pageable<Repetition>>;

  @Output()
  answerEvent = new EventEmitter<RepetitionModel>();

  repetition: Repetition;
  repetitionAnswerModel = new RepetitionAnswerModel();

  keyListeners: () => void;

  constructor(private renderer: Renderer2) {
  }

  ngOnInit(): void {
    this.repetitionAnswerModel.clear();
    this.repetition = this.repetitionsPageable.content.content[0];
  }

  ngOnDestroy(): void {
    this.destroyKeyListeners();
  }

  private moveFirstElementOnTheEndArray(): void {
    const removedFirstElement = this.repetitionsPageable.content.content.shift();
    this.repetitionsPageable.content.content.push(removedFirstElement);
  }

  public saveAnswer(): void {
    this.repetitionsPageable.content.content = this.repetitionsPageable.content.content
      .filter(repetition => repetition.repetitionId !== this.repetition.repetitionId);

    this.repetitionsPageable.content.totalElements = this.repetitionsPageable.content.totalElements - 1;
    this.nextRepetition(true);
  }

  nextRepetition(isValid: boolean): void {
    const loadMore = this.repetitionsPageable.content.content.length === 0;
    this.answerEvent.emit(new RepetitionModel(this.repetition.repetitionId, isValid, loadMore));

    if (!loadMore) {
      this.repetition = this.repetitionsPageable.content.content[0];
    }
    this.repetitionAnswerModel.clear();
  }

  skipRepetition(): void {
    this.nextRepetition(false);
  }

  hasRepetition(): boolean {
    return this.repetitionsPageable.error == null && this.repetitionsPageable.content.content.length > 0;
  }

  checkAnswers(): void {
    const isValidSecondStep = this.repetitionAnswerModel.validateAnswers(this.repetition.secondSide);
    if (isValidSecondStep) {
      this.registerKeyListener(event => this.onValidAnswerListener(event));
    } else {
      this.moveFirstElementOnTheEndArray();
      this.registerKeyListener(event => this.onInvalidAnswerListener(event));
    }
  }

  private registerKeyListener(eventFunction: (event: KeyboardEvent) => boolean): void {
    this.keyListeners = this.renderer.listen('document', 'keypress', event => {
      const isHandled = eventFunction(event);
      if (isHandled) {
        this.destroyKeyListeners();
      }
    });
  }

  private onInvalidAnswerListener(event: KeyboardEvent): boolean {
    if (event.key === '1') {
      this.skipRepetition();
      return true;
    } else if (event.key === '2') {
      this.saveAnswer();
      return true;
    }
    return false;
  }

  private onValidAnswerListener(event: KeyboardEvent): boolean {
    if (event.key === '1') {
      this.saveAnswer();
      return true;
    }
    return false;
  }

  private destroyKeyListeners(): void {
    if (this.keyListeners) {
      this.keyListeners();
    }
  }

  hasErrorMessage(): boolean {
    return this.repetitionsPageable.error !== null;
  }
}
