export class VerbRepetition {
  repetitionId: string;
  firstForm: string;
  secondForm: string;
  thirdForm: string;
  valid = false;
}

export class RepetitionResponse<T> {
  error: string;
  content: T;
}
export class Repetition {
  repetitionId: string;
  firstSide: string;
  secondSide: string;
  comment: string;
  valid = false;
}

export class RepetitionAnswer {
  constructor(
    private repetitionId: string,
    private valid: boolean
  ) {
  }
}

export class RepetitionModel {
  repetitionAnswer: RepetitionAnswer;
  loadMore = false;

  constructor(
    repetitionId: string,
    valid: boolean,
    loadMore: boolean
  ) {
    this.repetitionAnswer = new RepetitionAnswer(repetitionId, valid);
    this.loadMore = loadMore;
  }
}
