import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DefaultRepetitionComponent} from './default-repetition/default-repetition.component';
import {RepetitionComponent} from './repetition.component';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {NgbPopoverModule} from '@ng-bootstrap/ng-bootstrap';
import {VerbRepetitionComponent} from './verb-repetition/verb-repetition.component';
import {DefaultRepetitionViewComponent} from './default-repetition/view/default-repetition-view.component';
import {VerbRepetitionViewComponent} from './verb-repetition/view/verb-repetition-view.component';

const RepetitionRouter: Routes = [
  {
    path: '',
    component: RepetitionComponent,
    children: [
      {
        path: '',
        redirectTo: 'default',
        pathMatch: 'full'
      },
      {
        path: 'default',
        component: DefaultRepetitionComponent
      },
      {
        path: 'verb',
        component: VerbRepetitionComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    DefaultRepetitionViewComponent,
    RepetitionComponent,
    VerbRepetitionComponent,
    RepetitionComponent,
    DefaultRepetitionViewComponent,
    VerbRepetitionViewComponent,
    DefaultRepetitionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(RepetitionRouter),
    NgbPopoverModule
  ],
  exports: []
})
export class RepetitionModule {
}
