import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Repetition, RepetitionAnswer, RepetitionResponse, VerbRepetition} from '../model/Repetition';
import {BASE_URL, Pageable} from '../../shared/const';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RepetitionService {

  constructor(private httpClient: HttpClient) {
  }

  saveRepetition(repetitionAnswer: RepetitionAnswer): Observable<string> {
    return this.httpClient.post<string>(`${BASE_URL}/lang-repetitions`, repetitionAnswer);
  }

  getDefaultRepetitions(): Observable<RepetitionResponse<Pageable<Repetition>>> {
    return this.httpClient.get<RepetitionResponse<Pageable<Repetition>>>(`${BASE_URL}/lang-repetitions/default`);
  }

  getVerbFormRepetition(): Observable<VerbRepetition[]> {
    return this.httpClient.get<VerbRepetition[]>(`${BASE_URL}/lang-repetitions/verb-form`);
  }
}
