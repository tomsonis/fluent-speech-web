export class VerRepetitionModel {
  private _answeredFirstStep: boolean;
  private _validFirstStep: boolean;
  private _answeredSecondStep: boolean;
  private _validSecondStep: boolean;
  secondForm: string;
  thirdForm: string;

  private static trim(value: string): string {
    return !value ? '' : value.toLowerCase().trim();
  }

  get answeredFirstStep(): boolean {
    return this._answeredFirstStep;
  }

  get validFirstStep(): boolean {
    return this._validFirstStep;
  }

  get answeredSecondStep(): boolean {
    return this._answeredSecondStep;
  }

  get validSecondStep(): boolean {
    return this._validSecondStep;
  }

  answerFirstStep(isValid: boolean): void {
    this._answeredFirstStep = true;
    this._validFirstStep = isValid;
  }

  clear(): void {
    this._answeredFirstStep = false;
    this._answeredSecondStep = false;
    this._validFirstStep = false;
    this._validSecondStep = false;
    this.secondForm = null;
    this.thirdForm = null;
  }

  validateAnswers(secondForm: string, thirdForm: string, isRegular: boolean): boolean {
    this._answeredSecondStep = true;
    this._validSecondStep = VerRepetitionModel.trim(secondForm) === VerRepetitionModel.trim(this.secondForm);
    if (!isRegular) {
      this._validSecondStep = this._validSecondStep && VerRepetitionModel.trim(thirdForm) === VerRepetitionModel.trim(this.thirdForm);
    }
    return this.validSecondStep;
  }
}
