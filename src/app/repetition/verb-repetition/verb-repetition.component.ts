import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {RepetitionAnswer, VerbRepetition} from '../model/Repetition';
import {Observable} from 'rxjs';
import {RepetitionService} from '../service/repetition.service';

@Component({
  selector: 'app-verb-repetition',
  templateUrl: './verb-repetition.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VerbRepetitionComponent implements OnInit {

  verbRepetition$: Observable<VerbRepetition[]>;

  constructor(
    private repetitionService: RepetitionService
  ) {
  }

  ngOnInit(): void {
    this.loadMoreVerbRepetition();
  }

  loadMoreVerbRepetition(): void {
    this.verbRepetition$ = this.repetitionService.getVerbFormRepetition();
  }

  saveVerbRepetition(repetitionAnswer: RepetitionAnswer): void {
    this.repetitionService.saveRepetition(repetitionAnswer)
      .subscribe(_ => {});
  }
}
