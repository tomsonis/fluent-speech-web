import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RepetitionAnswer, VerbRepetition} from '../../model/Repetition';
import {VerRepetitionModel} from '../ver-repetition-model';

@Component({
  selector: 'app-verb-repetition-view',
  templateUrl: './verb-repetition-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VerbRepetitionViewComponent implements OnInit {

  @Input()
  verbRepetitions: VerbRepetition[];

  @Output()
  answerEvent = new EventEmitter<RepetitionAnswer>();

  @Output()
  loadMore = new EventEmitter<any>(false);

  repetition: VerbRepetition;
  verRepetitionModel = new VerRepetitionModel();

  ngOnInit(): void {
    this.prepareVerb();
  }

  prepareVerb(): void {
    this.verRepetitionModel.clear();
    this.repetition = this.verbRepetitions[0];
  }

  hasRepetition(): boolean {
    return this.verbRepetitions && this.verbRepetitions.length > 0;
  }

  checkIfIsRegular(): void {
    const isValidAnswer = this.isRegular();
    this.verRepetitionModel.answerFirstStep(isValidAnswer);
    if (!isValidAnswer) {
      this.answerEvent.emit(new RepetitionAnswer(this.repetition.repetitionId, false));
      this.moveFirstElementOnTheEndArray();
    }
  }

  isRegular(): boolean {
    return this.repetition.secondForm && !this.repetition.thirdForm;
  }

  private moveFirstElementOnTheEndArray(): void {
    const removedFirstElement = this.verbRepetitions.shift();
    this.verbRepetitions.push(removedFirstElement);
  }

  checkIfIsNotRegular(): void {
    const isValidAnswer = !this.isRegular();
    this.verRepetitionModel.answerFirstStep(isValidAnswer);
    if (!isValidAnswer) {
      this.answerEvent.emit(new RepetitionAnswer(this.repetition.repetitionId, false));
      this.moveFirstElementOnTheEndArray();
    }
  }

  checkAnswers(): void {
    const isValidSecondStep = this.verRepetitionModel.validateAnswers(this.repetition.secondForm, this.repetition.thirdForm, this.isRegular());
    if (!isValidSecondStep) {
      this.answerEvent.emit(new RepetitionAnswer(this.repetition.repetitionId, false));
      this.moveFirstElementOnTheEndArray();
    }
  }

  saveAnswer(): void {
    this.answerEvent.emit(new RepetitionAnswer(this.repetition.repetitionId, true));
    this.verbRepetitions = this.verbRepetitions.filter(verRepetition => verRepetition.repetitionId !== this.repetition.repetitionId);
    this.prepareVerb();
  }
}
