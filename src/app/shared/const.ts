import {environment} from '../../environments/environment';

export const BASE_URL = environment.backendUrl;

export enum Lang {
  EN = 'EN', PL = 'PL'
}


export interface Pageable<T> {
  content: T[];
  totalElements: number;
  totalPages: number;
  last: boolean;
  number: number;
  numberOfElements: number;
  first: boolean;
  size: number;
  empty: boolean;
}

export const DEFAULT_PAGE = 0;
export const DEFAULT_SIZE = 10;
