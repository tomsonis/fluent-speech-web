import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalObject} from '../ModalObject';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html'
})
export class DeleteComponent {

  @Input() modalObject: ModalObject;

  constructor(public modal: NgbActiveModal) {
  }
}
