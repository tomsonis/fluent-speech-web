import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Pageable} from '../const';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent {

  @Input()
  pageable: Pageable<any>;

  @Output()
  onPageSelectEvent = new EventEmitter<number>();

  totalPages = [];

  pagesArray(): number[] {
    return Array(this.pageable.totalPages).fill(0).map((_, i) => i);
  }

  onSelectPage(page: number): void {
    this.onPageSelectEvent.emit(page);
  }

  onNextPage(): void {
    if (this.pageable.number + 1 <= this.pageable.totalPages) {
      this.onPageSelectEvent.emit(this.pageable.number + 1);
    }
  }

  onPreviousPage(): void {
    if (this.pageable.number > 0) {
      this.onPageSelectEvent.emit(this.pageable.number - 1);
    }
  }
}
