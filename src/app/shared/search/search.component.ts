import {ChangeDetectionStrategy, Component, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged, filter, switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {DEFAULT_PAGE} from '../const';
import {SearchRequest} from './search.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchComponent {

  searchFormControl = new FormControl();

  @Output() search: Observable<SearchRequest>;

  constructor() {
    this.search = this.searchFormControl.valueChanges.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      filter(value => value && value.trim().length > 0),
      switchMap(value => of(new SearchRequest(value.trim().toLowerCase(), DEFAULT_PAGE)))
    );
  }
}
