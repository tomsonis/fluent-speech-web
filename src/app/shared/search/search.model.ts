export class SearchRequest {
  constructor(private _searchName: string, private _page: number) {
  }

  get searchName(): string {
    return this._searchName;
  }

  get page(): number {
    return this._page;
  }
}
