import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaginationComponent} from './pagination/pagination.component';
import {SearchComponent} from './search/search.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DeleteComponent} from './modal/delete/delete.component';
import {HttpClientModule} from '@angular/common/http';
import { ToastContainerComponent } from './toast-container/toast-container.component';
import { NgbToastModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [PaginationComponent, SearchComponent, DeleteComponent, ToastContainerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgbToastModule
  ],
  exports: [
    PaginationComponent,
    SearchComponent,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    ToastContainerComponent
  ]
})
export class SharedModule { }
