import {Component, TemplateRef} from '@angular/core';
import {AppToastService} from '../services/app-toast.service';

@Component({
  selector: 'app-toast-container',
  templateUrl: './toast-container.component.html',
  host: {'[class.fixed-center]': 'true'}
})
export class ToastContainerComponent {

  constructor(public toastService: AppToastService) {
  }

  isTemplate(toast): boolean { return toast.textOrTpl instanceof TemplateRef; }
}
